close all
clear 
clc

%% Load Model data
MODEL_DATA
MD.ModelName = 'SPM_dxf';

%% Build FEMM model
build_dxf2fem_model({'dxf/SPM_stator', 'dxf/SPM_rotor'}, MD, 1, 1, 1, 1)

%% Add boundary conditions
add_boundary2model(5, 10)

% Add material properties
add_materials2model(MD.Stator, MD.Rotor, mtrl_Ferrite, mtrl_Air)

%% Add slot labels
MD = add_slotlabels2model(MD);

mi_saveas([MD.ModelName, '.fem'])

