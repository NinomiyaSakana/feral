%% General data 
MD.ModelName = 'SPM_motor_complete'; % .fem model name 
MD.ScaleFactor = 1e-3; 
MD.StackLength = 100*MD.ScaleFactor; 
MD.PackFactor = 0.96; 
MD.PolePairs = 3; 
MD.Airgap = 0.6e-3; 
MD.SimPoles = 6; 

%% Stator general data 
Stator.Group = 1000; 
Stator.PolePairs = MD.PolePairs; 
% Stator.Geometry.FirstSlotAngle = 0;
Stator.Geometry.Airgap = MD.Airgap; 
Stator.Geometry.StackLength = MD.StackLength; 
Stator.Geometry.Slots = 36;  
Stator.Geometry.OuterDiameter = 180*MD.ScaleFactor; 
Stator.Geometry.InnerDiameter = 110*MD.ScaleFactor; 
Stator.Geometry.ToothWidth = 4.83*MD.ScaleFactor; 
Stator.Geometry.SlotHeight = 22.4*MD.ScaleFactor; 
Stator.Geometry.SlotOpeningHeight = 1*MD.ScaleFactor; 
Stator.Geometry.SlotOpeningWidth = 2.5*MD.ScaleFactor; 
Stator.Geometry.WedgeHeight = 1.5*MD.ScaleFactor; 
%% Stator materials 
Stator.Material.Slot = mtrl_Copper(150); 
Stator.Material.Lamination = mtrl_M530_50A(50); 
% Stator.Material.SlotOpening = mtrl_Air; 
%% Stator mesh sizes 
% Stator.Mesh.Lamination = 1*MD.ScaleFactor; 
% Stator.Mesh.Slot = 1*MD.ScaleFactor; 
% Stator.Mesh.SlotOpening = 1*MD.ScaleFactor 
%% Stator winding properties 
Stator.Winding.SlotFillFactor = 0.4;  
Stator.Winding.ConductorsInSlot = 24; 
Stator.Winding.ParallelPaths = 1; 
Stator.Winding.SlotMatrix = load('SlotMatrix.txt'); 
% Stator.Winding.CircName = 'Islot';

%% Rotor general data 
Rotor.Group = 10; 
Rotor.IronGroups = 11;
Rotor.MagnetGroups = Rotor.Group + [1:6]; % (for fft losses) 
Rotor.Alignment = 10; % [deg]
Rotor.PolePairs = MD.PolePairs; 
Rotor.Geometry.Airgap = MD.Airgap; 
Rotor.Geometry.StackLength = MD.StackLength; 
Rotor.Geometry.OuterDiameter = Stator.Geometry.InnerDiameter - 2*Stator.Geometry.Airgap; 
Rotor.Geometry.InnerDiameter = 40*MD.ScaleFactor; 
%% Rotor magnet dimensions 
% Rotor.Magnet.Geometry.Thickness = 2*MD.ScaleFactor; 
% Rotor.Magnet.Geometry.Width = 5*MD.ScaleFactor; 
%% Rotor materials 
Rotor.Material.Lamination = mtrl_M530_50A(150); 
Rotor.Material.Magnet = mtrl_N35SH(120); 
% Rotor.Material.Shaft = mtrl_Air; 
% Rotor.Material.Barrier = mtrl_Air; 
MD.Stator = Stator; 
MD.Rotor = Rotor; 
MD.MotionGroups = [MD.Rotor.Group, MD.Rotor.IronGroups, Rotor.MagnetGroups]; 

%% Set general simulation data 
% MD.FiguresFolder = 'figures'; 
% MD.ResultsFolder = 'results'; 
% MD.ModelPath = ''; 
% MD.ModelUnit = 'meters'; 
% MD.ModelType = 'planar'; 
% MD.ModelPrecision = 1e-8; 
% MD.MeshMinAngle = 30; 
% MD.ACSolver = 'Succ. Approx'; 
% MD.FemmVisibility = 0; % set the FEMM visibility 
% MD.NewFemmInstance = 0; % open a new FEMM instance 
% MD.MechSpeedRPM = 1000; % declare mechanical speed [rpm] 
% MD.Skew_vec = [0]; % declare skewing angle array 
% MD.RotorPositions = [0]; % declare rotor position array 
% MD.AirgapMeshSize = 0.25e-3; % declare air-gap mesh size [m] 
% MD.EndWindingLength = 0; % declare end-winding length [m] 
% MD.EndWindingInductance = 0; % declare end-winding inductance [H] 
% MD.AirgapFluxDensityContourPoints = 720; % number of samples of airgap flux density 
% MD.AirgapFluxDensityAngle = 360/2/PolePairs; % angle of airgap flux density 
% MD.IronLossesFFT = 0; % abilitate iron losses fft computation 
% MD.MirrorHalfPeriod = 0; % mirror an electric period 
% MD.SaveMeshNodes = 0; % save the mesh nodes coordinates 
% MD.SaveMeshElementsValues = 0; % save mesh elements properties 
% MD.AddLossesCoeff = 0; % increase the total losses 
% MD.PackFactor = 1; % packaging factor 
% MD.DisplayProgress = 1; % display the simulation progress percentage 
% MD.SaveResults = 1; % save the simulation results 
% MD.FileResultsPrefix = 'something'; % add a prefix to results file name 
