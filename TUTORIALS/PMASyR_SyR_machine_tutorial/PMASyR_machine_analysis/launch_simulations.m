close all
clear 
clc

%% Single no-load simulation 
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 0; % [A]
SD.CurrentAngle = 0; % [deg]
sim_var_rotor_position(MD, SD);

%% Single no-load simulation with custom temp folder path
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 0; % [A]
SD.CurrentAngle = 0; % [deg]
SD.TempFolderPath = '..\'; % <-- put here a path of your PC
SD.TempFolderName = 'HERE_I_AM';
Results = sim_var_rotor_position(MD, SD);

%% Single no-load simulation with physical rotation
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 0; % [A]
SD.CurrentAngle = 10; % [deg]
SD.MoveMotionGroups = 1; % enable physical rotor rotation
Results = sim_var_rotor_position(MD, SD);

%% Single no-load simulation with density plot saving
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 0; % [A]
SD.CurrentAngle = 10; % [deg]
SD.MoveMotionGroups = 1; % enable physical rotor rotation
SD.SaveDensityPlots = 1;
SD.ImageTransparency = 0; % try 1 or 0 and see the picture
Results = sim_var_rotor_position(MD, SD);

%% Single no-load simulation with density plot saving and auto crop
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 0; % [A]
SD.CurrentAngle = 10; % [deg]
SD.MoveMotionGroups = 1; % enable physical rotor rotation
SD.SaveDensityPlots = 1; % enable picture saving
SD.DensityPlotLegend = 0; % remove legend
SD.ImageCrop = 1; % enable image crop
SD.ImageTransparency = 1; % remove white background
Results = sim_var_rotor_position(MD, SD);

%% No-Load simulation (0-60deg)
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 0; % [A]
SD.CurrentAngle = 0; % [deg]
SD.RotorPositions = [0:3:60-3]/MD.PolePairs; % [deg]
SD.FileResultsPrefix = 'no_load';

[AvgValues, VecValues] = sim_var_rotor_position(MD, SD);
AvgValues.TotalSimulationTime

%% No-Load simulation (0-60deg) (without UsePreviousSolution)
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 0; % [A]
SD.CurrentAngle = 0; % [deg]
SD.RotorPositions = [0:3:60-3]/MD.PolePairs; % [deg]
SD.FileResultsPrefix = 'no_load';
SD.UsePreviousSolution = 0; % disabled

[AvgValues, VecValues] = sim_var_rotor_position(MD, SD);
AvgValues.TotalSimulationTime

%% No-Load simulation (0-60deg) (plot results)
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 0; % [A]
SD.CurrentAngle = 0; % [deg]
SD.RotorPositions = [0:3:360-3]/MD.PolePairs; % [deg]
SD.FileResultsPrefix = 'no_load';
SD.PlotResults = 1; % enable results plotting
SD.SaveFigures = 1; % enable figures saving

[AvgValues, VecValues] = sim_var_rotor_position(MD, SD);

%% No-Load simulation (0-60deg) extended plots and plot results
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 0; % [A]
SD.CurrentAngle = 0; % [deg]
SD.RotorPositions = [0:3:60-3]/MD.PolePairs; % [deg]
SD.FileResultsPrefix = 'no_load';
SD.AirgapFluxDensityFigure = [0, 1.5, 3];
SD.PlotResults = 1; % enable results plotting
SD.SaveFigures = 1; % enable figures saving
SD.CompletePeriod = 1; % enable completion of the electric period

[AvgValues, VecValues] = sim_var_rotor_position(MD, SD);

%% No-Load Model with element by element material computation
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 0;
SD.CurrentAngle = 0;
SD.RotorPositions = 0;
SD.GetElmByElmProp = 1;

[AvgValues, VecValues] = sim_var_rotor_position(MD, SD);

%% No-Load Model data iron losses
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 0;
SD.CurrentAngle = 0;
SD.RotorPositions = [0:30:180-30]/MD.PolePairs;
SD.FileResultsPrefix = 'fft_ironlosses';
SD.IronLossesFFT = 1; % compute fft losses
SD.MirrorHalfPeriod = 1; % extend the electric period
SD.SaveMeshNodes = 1; % save the mesh nodes
SD.SaveMeshElementsValues = 1; % save the element properties
SD.TempID = 'FFT'; % rename the temporary folder name
SD.MechSpeedRPM = 3000; % [rpm] mechanical speed 

[AvgValues, VecValues] = sim_var_rotor_position(MD, SD);

%% On-load simulation (0-60deg) 
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 60; % [A]
SD.CurrentAngleRange = 45:2.5:60;
mtpa = sim_find_mtpa(MD, SD)

SD.CurrentAngle = mtpa.CurrentAngle; % [deg]
SD.RotorPositions = [0:3:60-3]/MD.PolePairs; % [deg]
SD.FileResultsPrefix = 'on_load';
SD.FiguresFolder = 'on_load_figures';
SD.CompletePeriod = 1;
SD.PlotResults = 1;
SD.SaveFigures = 1;

[AvgValues, VecValues] = sim_var_rotor_position(MD, SD);

%% On-load simulation (0-60deg) with skewing
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 60; % [A]
SD.CurrentAngleRange = 45:2.5:60;
mtpa = sim_find_mtpa(MD, SD)

SD.CurrentAngle = mtpa.CurrentAngle; % [deg]
SD.RotorPositions = [0:3:60-3]/MD.PolePairs; % [deg]
SD.FileResultsPrefix = 'on_load';
SD.SkewingAngles = [-10/3, 0, 10/3];
SD.CompletePeriod = 1;
SD.PlotResults = 1;
SD.SaveFigures = 1;

[AvgValues, VecValues] = sim_var_rotor_position(MD, SD);

%% On-load simulation (0-60deg) with density plots 
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 67; % [A]
SD.CurrentAngle = 57; % [deg]
SD.MoveMotionGroups = 1; 
SD.SaveDensityPlots = 1; % enable plot density saving
SD.DensityPlotType = {'bmag','jmag'}; % plot both flux and current density plots
SD.DensityPlotGray = [1, 0]; % bmag is colored, jmag has gray scale
SD.DensityPlotLegend = [0, 0]; % no legend
SD.DensityPlotMax = [2, 3]; % maximum values for bmag and jmag
SD.DensityPlotMin = [0, 0]; % minimum values for bmag and jmag
SD.ImageCrop = 1;
SD.ImageTransparency = 1;

sim_var_rotor_position(MD, SD);

%% On-load simulation (0-60deg) extended
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 80; % [A]
SD.CurrentAngleRange = [90:100]; % [deg] 

% find the MTPA current angle
mtpa = sim_find_mtpa(MD, SD);

SD.CurrentAngle = mtpa.CurrentAngle; % [deg]
SD.RotorPositions = [0:3:60-3]/MD.PolePairs; % [deg]
SD.FileResultsPrefix = 'on_load';
SD.PlotResults = 1;
SD.SaveFigures = 1;
SD.MechSpeedRPM = 2000; % [rpm]
SD.CompletePeriod = 1;

[AvgValues, VecValues] = sim_var_rotor_position(MD, SD);

%% Series of On-load simulations (0-60deg) extended
clc, clear SD
MODEL_DATA

% some simulation settings
SD.FileResultsPrefix = 'on_load';
SD.RotorPositions = [0:3:60-3]/MD.PolePairs; % [deg]
SD.CompletePeriod = 1; % extend period
SD.PlotResults = 1; % plot results
SD.SaveFigures = 1; % save pdf figures
SD.FiguresFolder = 'all_on_load_figures';
SD.CloseFigures = 1; % close the plot windows 
SD.TempFolderName = 'many_on_load_simulations';
SD.ResultsFolder = 'all_on_load_results';

% working points to test (random number!)
CurrentAmplitude = [12, 25, 38, 49, 57, 68, 73, 80]; % [A]
CurrentAngle = [45, 50, 53, 68, 75, 80, 85, 90]; % [deg]
MechSpeedRPM = [1, 2, 0.5, 3, 2.5, 1.5, 6, 5.5]*1e3; % [rpm]

% initialize total structure
AllResults(length(CurrentAmplitude)).AvgValues = struct;

for jj = 1:length(CurrentAmplitude)
  
  clc
  jj
  SD.CurrentAmplitude = CurrentAmplitude(jj); % [A]
  SD.CurrentAngle = CurrentAngle(jj); % [deg] 
  SD.MechSpeedRPM = MechSpeedRPM(jj); % [rpm]

  AllResults(jj).AvgValues = sim_var_rotor_position(MD, SD);

end

% Save the total structure array 
% save('total_simulations_results.mat', 'AllResults')

%% Mapping
clc, clear SD
MODEL_DATA

SD.Id_vec = 0:10:80; % [A]
SD.Iq_vec = 0:10:80; % [A]
%SD.RotorPositions = [0:3:60]/MD.PolePairs;
%SD.SkewingAngles = [0];
SD.TempID = 'map';
SD.ClearTempFolder = 1;
SD.PlotResults = 1;


MyFirstMap = sim_mapping(MD, SD);

%% Mapping with skewing
clc, clear SD
MODEL_DATA

SD.Id_vec = 0:5:80; % [A]
SD.Iq_vec = 0:5:80; % [A]
SD.SkewingAngles = [-10/3 0 10/3];
SD.TempID = 'map';
SD.ClearTempFolder = 1;
SD.PlotResults = 1;

[Map, SkewMap] = sim_mapping(MD, SD);


%% Mapping with FFT iron losses
clc, clear SD
MODEL_DATA

SD.Id_vec = 0:10:80; % [A]
SD.Iq_vec = 0:10:80; % [A]
SD.RotorPositions = [0:3:180-3]/MD.PolePairs;
SD.IronLossesFFT = 1; % compute fft losses
SD.MirrorHalfPeriod = 1; % extend the electric period
SD.TempID = 'map_fft';

[Map, SkewMap] = sim_mapping(MD, SD);
