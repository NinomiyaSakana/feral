%% General data 
MD.ModelName = 'PMASyR_machine_1pole'; % .fem model name 
MD.ModelPath = '../';
MD.ScaleFactor = 1e-3; 
MD.StackLength = 100*MD.ScaleFactor; 
MD.PackFactor = 0.96; 
MD.PolePairs = 2; 
MD.Airgap = 0.4e-3; 
MD.SimPoles = 1; 

%% Stator general data 
Stator.Group = 1000; 
Stator.PolePairs = MD.PolePairs; 
Stator.Geometry.Airgap = MD.Airgap; 
Stator.Geometry.StackLength = MD.StackLength; 
Stator.Geometry.Slots = 36;  
Stator.Geometry.OuterDiameter = 150*MD.ScaleFactor; 
Stator.Geometry.InnerDiameter = 96*MD.ScaleFactor; 
Stator.Geometry.ToothWidth = 4.45*MD.ScaleFactor; 
Stator.Geometry.SlotHeight = 13.85*MD.ScaleFactor; 
Stator.Geometry.SlotOpeningHeight = 1*MD.ScaleFactor;

%% Stator materials 
Stator.Material.Slot = mtrl_Copper(150); 
Stator.Material.Lamination = mtrl_M530_50A(150); 

%% Stator winding properties 
Stator.Winding.SlotFillFactor = 0.4;  
Stator.Winding.ConductorsInSlot = 8; 
Stator.Winding.ParallelPaths = 2; 
Stator.Winding.SlotMatrix = load('..\SlotMatrix.txt'); 

%% Rotor general data 
Rotor.Group = 10; 
Rotor.IronGroup = 11;
Rotor.MagnetGroups = 12; % (for fft losses) 
Rotor.Alignment = 20-45; % [deg]
Rotor.PolePairs = MD.PolePairs; 
Rotor.Geometry.Airgap = MD.Airgap; 
Rotor.Geometry.StackLength = MD.StackLength; 
Rotor.Geometry.OuterDiameter = Stator.Geometry.InnerDiameter - 2*Stator.Geometry.Airgap; 
Rotor.Geometry.InnerDiameter = 36*MD.ScaleFactor; 

%% Rotor materials 
Rotor.Material.Lamination = mtrl_M530_50A(150); 
Rotor.Material.Magnet = mtrl_Ferrite(120); 

%% Gather stator and rotor in MD
MD.Stator = Stator; 
MD.Rotor = Rotor; 
MD.MotionGroups = [MD.Rotor.Group, Rotor.IronGroup, Rotor.MagnetGroups]; 

%% General plot settings
% change phase names
MD.Legend.PhaseA = 'X';
MD.Legend.PhaseB = 'Y';
MD.Legend.PhaseC = 'Z';

% set custom color for some plots
MD.Color.PhaseA = [200,55,113]/255;
MD.Color.PhaseB = [55,200,55]/255;
MD.Color.PhaseC = [85,153,255]/255;
MD.Color.FluxDensity = [255,69,255]/255;
MD.Color.TorqueMXW = [0,0,0];
MD.Color.TorqueDQ = [0,0,1];
MD.Color.MTPA = [127,0,255]/255;
MD.Color.FW = [255,0,255]/255;
MD.Color.MTPV = [255,128,0]/255;
MD.Color.Ilim = [0,0,0]/255;

% Reduce linewidth for B and C phases
MD.LineWidth.PhaseB = 1;
MD.LineWidth.PhaseC = 1;

% Change markers in torque curves
MD.LineStyle.TorqueMXW = '-o';
MD.LineStyle.TorqueDQ = '-';

% Change font name for your papers
MD.FontName.Label = 'Times';
MD.FontName.Legend = 'Times';
MD.FontName.Axis = 'Times';

% Set bold axis labels
MD.FontWeight.Axis = 'bold';

% Change no-cross saturation legend
MD.Legend.FluxD0 = 'no q-axis current';
MD.Legend.FluxQ0 = 'no d-axis current';

% Set axis label font size
MD.FontSize.Axis = 12;

