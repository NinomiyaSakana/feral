# FERAL

Matlab/Octave library to analyze synchronous machines with FEMM.

**05/02/2020 New FERAL features**

* Added function `calc_torque_speed_working_point` to compute the working point (Id, Iq) which yields a given torque at a given speed within the current and voltage limits (BETA version)
* Added phase and line to line voltages in simulation outputs
* Added and fixed some minor functions

**21/01/2020**

* Synchronous machines with **INTERNAL** stator can be simulated now!
* Custom currents can be used as input of the simulations: the currents can be defined by data or a string formula.
  The option `CurrentWaveformType` can be: 

    1.  `sin` for sinusoidal currents (as usual)
    2. `array` for currents defined by data
    3. `formula` for currents defined by formula with variable `thetam` (mechanical rotor position)


* The option `DiameterHandle` has been added both for `Stator.Geometry` and `Rotor.Geometry` to use models whose reference airgap diameter (necessary to draw the airgap lines) is 
  different from the actual one
* Function `calc_slot_matrix` has been added to compute the slot matrix of a winding
* Function `draw_gap_lines` updated (important)
* Some material properties have been fixed
* *Stay tuned for the updated FERAL documentation* ...


