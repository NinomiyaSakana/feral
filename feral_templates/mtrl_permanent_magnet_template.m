function material = mtrl_permanent_magnet_template(Temperature)
%MTRL_N35SH defines the properties of the 'N35SH' material

FunctionName = mfilename;
MaterialName = FunctionName(6:end);

% set default temperature is not defined
if nargin < 1
  Temperature = 20; % [degree Celsius]
end

material.Name = [MaterialName,'@',num2str(Temperature),'C'];

material.TemperatureRef = ; % [degree Celsius]
material.RelativePermeability = [];
material.RemanenceRef = ; % [T]
material.RemanenceTemperatureCoeff = ; % [%Br/(degree Celsius)]  
material.Temperature = Temperature; % [degree Celsius]


% compute the permanent magnet remanence at 'Temperature'
material.Remanence = calc_remanence_at_temperature( material.RemanenceRef, ...
                                                    material.RemanenceTemperatureCoeff, ...
                                                    material.TemperatureRef, ...
                                                    material.Temperature); % [T]

material.CoerciveField = material.Remanence / (4e-7*pi * material.RelativePermeability(1)); % [A/m]

% Additional properties
material.ElResistivity= ; % [ohm * m]
material.ElConductivity = ; % [MS / m]
material.MassDensity = ; % [kg/m3]

end % function