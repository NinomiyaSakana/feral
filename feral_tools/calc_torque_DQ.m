function Torque = calc_torque_DQ(PolePairs, Id, Iq, FluxD, FluxQ)
%%CALC_TORQUE_DQ compute the D-Q torque value

Torque = 3/2 * PolePairs * (FluxD .* Iq - FluxQ .* Id);

end % function